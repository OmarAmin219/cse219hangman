package gui;

import apptemplate.AppTemplate;
import components.AppWorkspaceComponent;
import controller.HangmanController;
import javafx.collections.ObservableList;
import javafx.geometry.Pos;
import javafx.scene.Group;
import javafx.scene.Node;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ToolBar;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.scene.layout.VBox;
import javafx.scene.shape.ArcType;
import propertymanager.PropertyManager;
import ui.AppGUI;

import java.io.IOException;

import static hangman.HangmanProperties.*;

/**
 * This class serves as the GUI component for the Hangman game.
 *
 * @author Ritwik Banerjee
 * @author Omar Amin
 */
public class Workspace extends AppWorkspaceComponent {

    AppTemplate app; // the actual application
    AppGUI      gui; // the GUI inside which the application sits
    Label             guiHeadingLabel;   // workspace (GUI) heading label
    HBox              headPane;          // conatainer to display the heading
    HBox              bodyPane;          // container for the main game displays
    ToolBar           footToolbar;       // toolbar for game buttons
    BorderPane        figurePane;        // container to display the namesake graphic of the (potentially) hanging person
    VBox              gameTextsPane;     // container to display the text-related parts of the game
    HBox              guessedLetters;    // text area displaying all the letters guessed so far
    HBox              remainingGuessBox; // container to display the number of remaining guesses
    Button            startGame;         // the button to start playing a game of Hangman
    HangmanController controller;
    HBox              alphabet;
    Button            hint;



    /**
     * Constructor for initializing the workspace, note that this constructor
     * will fully setup the workspace user interface for use.
     *
     * @param initApp The application this workspace is part of.
     * @throws IOException Thrown should there be an error loading application
     *                     data for setting up the user interface.
     */

    public BorderPane getFigurePane(){
        return figurePane;
    }
    public Workspace(AppTemplate initApp) throws IOException {
        app = initApp;
        gui = app.getGUI();
        controller = (HangmanController) gui.getFileController();    //new HangmanController(app, startGame); <-- THIS WAS A MAJOR BUG!??

        layoutGUI();     // initialize all the workspace (GUI) components including the containers and their layout

        setupHandlers(); // ... and set up event handling


    }

    private void layoutGUI() {
        PropertyManager propertyManager = PropertyManager.getManager();
        guiHeadingLabel = new Label(propertyManager.getPropertyValue(WORKSPACE_HEADING_LABEL));

        headPane = new HBox();
        headPane.getChildren().add(guiHeadingLabel);
        headPane.setAlignment(Pos.CENTER);

        figurePane = new BorderPane();
        figurePane.setPrefHeight(400);
        figurePane.setPrefWidth(400);

        hint = new Button("Hint");

        alphabet = new HBox();
        makeAlphabet();
        /*hint.setOnMouseClicked(e -> controller.handleHint());
        hint.setOnMousePressed(e -> controller.handleHint());*/
        guessedLetters = new HBox();
        guessedLetters.setStyle("-fx-background-color: transparent;");
        remainingGuessBox = new HBox();
        gameTextsPane = new VBox();

        gameTextsPane.getChildren().setAll(remainingGuessBox, guessedLetters,alphabet,hint);

        bodyPane = new HBox();
        bodyPane.getChildren().addAll(figurePane, gameTextsPane);

        startGame = new Button("Start Playing");
        HBox blankBoxLeft  = new HBox();
        HBox blankBoxRight = new HBox();
        HBox.setHgrow(blankBoxLeft, Priority.ALWAYS);
        HBox.setHgrow(blankBoxRight, Priority.ALWAYS);
        footToolbar = new ToolBar(blankBoxLeft, startGame, blankBoxRight);

        workspace = new VBox();
        workspace.getChildren().addAll(headPane, bodyPane, footToolbar);


    }

    private void setupHandlers() {
        startGame.setOnMouseClicked(e -> {controller.start(); alphabet.requestFocus();});
        hint.setOnMouseClicked(e -> controller.handleHint());



    }

    public void drawHangMan(){

    }




    /**
     * This function specifies the CSS for all the UI components known at the time the workspace is initially
     * constructed. Components added and/or removed dynamically as the application runs need to be set up separately.
     */
    @Override
    public void initStyle() {
        PropertyManager propertyManager = PropertyManager.getManager();

        gui.getAppPane().setId(propertyManager.getPropertyValue(ROOT_BORDERPANE_ID));
        gui.getToolbarPane().getStyleClass().setAll(propertyManager.getPropertyValue(SEGMENTED_BUTTON_BAR));
        gui.getToolbarPane().setId(propertyManager.getPropertyValue(TOP_TOOLBAR_ID));

        ObservableList<Node> toolbarChildren = gui.getToolbarPane().getChildren();
        toolbarChildren.get(0).getStyleClass().add(propertyManager.getPropertyValue(FIRST_TOOLBAR_BUTTON));
        toolbarChildren.get(toolbarChildren.size() - 1).getStyleClass().add(propertyManager.getPropertyValue(LAST_TOOLBAR_BUTTON));

        workspace.getStyleClass().add(CLASS_BORDERED_PANE);
        guiHeadingLabel.getStyleClass().setAll(propertyManager.getPropertyValue(HEADING_LABEL));

    }

    /** This function reloads the entire workspace */
    @Override
    public void reloadWorkspace() {
        /* does nothing; use reinitialize() instead */
    }

    public VBox getGameTextsPane() {
        return gameTextsPane;
    }

    public HBox getRemainingGuessBox() {
        return remainingGuessBox;
    }

    public Button getStartGame() {
        return startGame;
    }

    public void makeAlphabet(){
        Button a = new Button("A");
        Button b = new Button("B");
        Button c = new Button("C");
        Button d = new Button("D");
        Button E = new Button("E");
        Button f = new Button("F");
        Button g = new Button("G");
        Button h = new Button("H");
        Button i = new Button("I");
        Button j = new Button("J");
        Button k = new Button("K");
        Button l = new Button("L");
        Button m = new Button("M");
        Button n = new Button("N");
        Button o = new Button("O");
        Button p = new Button("P");
        Button q = new Button("Q");
        Button r = new Button("R");
        Button s = new Button("S");
        Button t = new Button("T");
        Button u = new Button("U");
        Button v = new Button("V");
        Button w = new Button("W");
        Button x = new Button("X");
        Button y = new Button("Y");
        Button z = new Button("Z");
        alphabet.getChildren().addAll(a,b,c,d,E,f,g,h,i,j,k,l,m,n,o,p,q,r,s,t,u,v,w,x,y,z);

        alphabet.setOnKeyPressed(e -> {
            switch (e.getCode()) {
                case A:
                    a.setStyle("-fx-background-color: transparent");
                    a.setText("");
                    break;
                case B:
                    b.setStyle("-fx-background-color: transparent");
                    b.setText("");
                    break;
                case C:
                    c.setStyle("-fx-background-color: transparent");
                    c.setText("");
                    break;
                case D:
                    d.setStyle("-fx-background-color: transparent");
                    d.setText("");
                    break;
                case E:
                    E.setStyle("-fx-background-color: transparent");
                    E.setText("");
                    break;
                case F:
                    f.setText("");
                    f.setStyle("-fx-background-color: transparent");
                    break;
                case G:
                    g.setText("");
                    g.setStyle("-fx-background-color: transparent");
                    break;
                case H:
                    h.setText("");
                    h.setStyle("-fx-background-color: transparent");
                    break;
                case I:
                    i.setText("");
                    i.setStyle("-fx-background-color: transparent");
                    break;
                case J:
                    j.setText("");
                    j.setStyle("-fx-background-color: transparent");
                    break;
                case K:
                    k.setText("");
                    k.setStyle("-fx-background-color: transparent");
                    break;
                case L:
                    l.setText("");
                    l.setStyle("-fx-background-color: transparent");
                    break;
                case M:
                    m.setText("");
                    m.setStyle("-fx-background-color: transparent");
                    break;
                case N:
                    n.setText("");
                    n.setStyle("-fx-background-color: transparent");
                    break;
                case O:
                    o.setText("");
                    o.setStyle("-fx-background-color: transparent");
                    break;
                case P:
                    p.setText("");
                    p.setStyle("-fx-background-color: transparent");
                    break;
                case Q:
                    q.setText("");
                    q.setStyle("-fx-background-color: transparent");
                    break;
                case R:
                    r.setText("");
                    r.setStyle("-fx-background-color: transparent");
                    break;
                case S:
                    s.setText("");
                    s.setStyle("-fx-background-color: transparent");
                    break;
                case T:
                    t.setText("");
                    t.setStyle("-fx-background-color: transparent");
                    break;
                case U:
                    u.setText("");
                    u.setStyle("-fx-background-color: transparent");
                    break;
                case V:
                    v.setText("");
                    v.setStyle("-fx-background-color: transparent");
                    break;
                case W:
                    w.setText("");
                    w.setStyle("-fx-background-color: transparent");
                    break;
                case X:
                    x.setText("");
                    x.setStyle("-fx-background-color: transparent");
                    break;
                case Y:
                    y.setText("");
                    y.setStyle("-fx-background-color: transparent");
                    break;
                case Z:
                    z.setText("");
                    z.setStyle("-fx-background-color: transparent");
                    break;
            }
        });


    }

    public Button getHint(){
        return hint;
    }

    public HBox getAlphabetBox(){
        return alphabet;
    }

    public void reinitialize() {
        guessedLetters = new HBox();
        guessedLetters.setStyle("-fx-background-color: transparent;");
        remainingGuessBox = new HBox();
        figurePane = new BorderPane();
        figurePane.setPrefHeight(400);
        figurePane.setPrefWidth(400);
        alphabet = new HBox();
        hint = new Button("Hint");
        hint.setVisible(false);
        hint.setOnMouseClicked(e -> controller.handleHint());

        makeAlphabet();
        gameTextsPane = new VBox();
        gameTextsPane.getChildren().setAll(remainingGuessBox, guessedLetters,alphabet,hint);
        bodyPane.getChildren().setAll(figurePane, gameTextsPane);


    }
}