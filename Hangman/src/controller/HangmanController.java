package controller;

import apptemplate.AppTemplate;
import data.GameData;
import gui.Workspace;
import javafx.animation.AnimationTimer;
import javafx.application.Platform;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.paint.Color;
import javafx.scene.paint.Paint;
import javafx.scene.shape.Circle;
import javafx.scene.shape.Line;
import javafx.scene.shape.Shape;
import javafx.scene.text.Text;
import javafx.stage.FileChooser;
import javafx.stage.FileChooser.ExtensionFilter;
import propertymanager.PropertyManager;
import ui.AppMessageDialogSingleton;
import ui.YesNoCancelDialogSingleton;

import java.io.File;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Random;
import java.util.Set;

import static settings.AppPropertyType.*;
import static settings.InitializationParameters.APP_WORKDIR_PATH;

/**
 * @author Ritwik Banerjee
 * @author omar amin
 */
public class HangmanController implements FileController {

    public enum GameState {
        UNINITIALIZED,
        INITIALIZED_UNMODIFIED,
        INITIALIZED_MODIFIED,
        ENDED
    }

    private AppTemplate appTemplate; // shared reference to the application
    private GameData    gamedata;    // shared reference to the game being played, loaded or saved
    private GameState   gamestate;   // the state of the game being shown in the workspace
    private Text[]      progress;    // reference to the text area for the word
    private boolean     success;     // whether or not player was successful
    private int         discovered;  // the number of letters already discovered
    private Button      gameButton;  // shared reference to the "start game" button
    private Label       remains;     // dynamically updated label that indicates the number of remaining guesses
    private Path        workFile;
    private Button[]    alphabet;    //used to hold the alphabet
    private Shape[]     hangmanparts;
    boolean hintProcessed = false;

    public HangmanController(AppTemplate appTemplate, Button gameButton) {
        this(appTemplate);
        this.gameButton = gameButton;
    }

    public HangmanController(AppTemplate appTemplate) {
        this.appTemplate = appTemplate;
        this.gamestate = GameState.UNINITIALIZED;

    }

    public void enableGameButton() {
        if (gameButton == null) {
            Workspace workspace = (Workspace) appTemplate.getWorkspaceComponent();
            gameButton = workspace.getStartGame();
        }
        gameButton.setDisable(false);
    }

    public void disableGameButton() {
        if (gameButton == null) {
            Workspace workspace = (Workspace) appTemplate.getWorkspaceComponent();
            gameButton = workspace.getStartGame();
        }
        gameButton.setDisable(true);
    }

    public void setGameState(GameState gamestate) {
        this.gamestate = gamestate;
    }

    public GameState getGamestate() {
        return this.gamestate;
    }

    /**
     * In the homework code given to you, we had the line
     * gamedata = new GameData(appTemplate, true);
     * This meant that the 'gamedata' variable had access to the app, but the data component of the app was still
     * the empty game data! What we need is to change this so that our 'gamedata' refers to the data component of
     * the app, instead of being a new object of type GameData. There are several ways of doing this. One of which
     * is to write (and use) the GameData#init() method.
     */
    public void start() {
        gamedata = (GameData) appTemplate.getDataComponent();
        success = false;
        discovered = 0;

        Workspace gameWorkspace = (Workspace) appTemplate.getWorkspaceComponent();

        gamedata.init();
        setGameState(GameState.INITIALIZED_UNMODIFIED);
        HBox remainingGuessBox = gameWorkspace.getRemainingGuessBox();
        HBox guessedLetters    = (HBox) gameWorkspace.getGameTextsPane().getChildren().get(1);
        BorderPane figurePane = gameWorkspace.getFigurePane();

        if (gamedata.isDifficult()){
            gameWorkspace.getHint().setVisible(true);


        }
        remains = new Label(Integer.toString(GameData.TOTAL_NUMBER_OF_GUESSES_ALLOWED));
        remainingGuessBox.getChildren().addAll(new Label("Remaining Guesses: "), remains);
        initWordGraphics(guessedLetters,figurePane);

        play();
    }

    private void end() {
        appTemplate.getGUI().getPrimaryScene().setOnKeyTyped(null);
        gameButton.setDisable(true);
        setGameState(GameState.ENDED);
        appTemplate.getGUI().updateWorkspaceToolbar(gamestate.equals(GameState.INITIALIZED_MODIFIED));
        for (int i = 0; i < alphabet.length; i++){
            if (alphabet[i].getStyle().equals("-fx-background-color: black")) {
                alphabet[i].setTextFill(Color.WHITE);

            }

        }
        Platform.runLater(() -> {
            PropertyManager           manager    = PropertyManager.getManager();
            AppMessageDialogSingleton dialog     = AppMessageDialogSingleton.getSingleton();
            String                    endMessage = manager.getPropertyValue(success ? GAME_WON_MESSAGE : GAME_LOST_MESSAGE);
            /*if (!success)
                endMessage += String.format(" (the word was \"%s\")", gamedata.getTargetWord());*/
            if (dialog.isShowing())
                dialog.toFront();
            else
                dialog.show(manager.getPropertyValue(GAME_OVER_TITLE), endMessage);
        });

    }

    private void initWordGraphics(HBox guessedLetters, BorderPane figurePane ) {
        char[] targetword = gamedata.getTargetWord().toCharArray();
        progress = new Text[targetword.length];
        alphabet = new Button[targetword.length];
        hangmanparts = new Shape[10];


        for (int i = 0; i < progress.length; i++) {
            progress[i] = new Text(Character.toString(targetword[i]));
            progress[i].setStyle("-fx-background-color: black");
            progress[i].setVisible(false);
            alphabet[i] = new Button(Character.toString(targetword[i]));
            alphabet[i].setStyle("-fx-background-color: black");
            alphabet[i].setTextFill(Color.BLACK);

        }
        Circle head = new Circle();

        head.setCenterX(200);
        head.setCenterY(150);
        head.setRadius(50);
        hangmanparts[4] = head;
        Line line1 = new Line(50,350,150,350);
        hangmanparts[0] = line1;
        Line line2 = new Line(100,350,100,50);
        hangmanparts[1] = line2;
        Line line3 = new Line(100,50,200,50);
        hangmanparts[2] = line3;
        Line line4 = new Line(200,50,200,100);
        hangmanparts[3] = line4;
        Line line5 = new Line(200,200,200,300);
        hangmanparts[5] = line5;
        Line line6 = new Line(200,300,150,350);
        hangmanparts[6] = line6;
        Line line7 = new Line(200,300,250,350);
        hangmanparts[7] = line7;
        Line line8 = new Line(200,250,150,300);
        hangmanparts[8] = line8;
        Line line9 = new Line(200,250,250,300);
        hangmanparts[9] = line9;
        for (int i = 0; i < 10; i++) {
            hangmanparts[i].setFill(Color.TRANSPARENT);
            hangmanparts[i].setStroke(Color.TRANSPARENT);
        }
        guessedLetters.getChildren().addAll(alphabet);
        figurePane.getChildren().addAll(hangmanparts);
    }



    public void play() {
        disableGameButton();
        AnimationTimer timer = new AnimationTimer() {
            @Override
            public void handle(long now) {
                appTemplate.getGUI().updateWorkspaceToolbar(gamestate.equals(GameState.INITIALIZED_MODIFIED));
                appTemplate.getGUI().getPrimaryScene().setOnKeyTyped((KeyEvent event) -> {
                    char guess = event.getCharacter().charAt(0);
                    if ((guess >= 'a' && guess <= 'z')){
                    if (!alreadyGuessed(guess)) {
                        boolean goodguess = false;
                        for (int i = 0; i < progress.length; i++) {
                            if (gamedata.getTargetWord().charAt(i) == guess) {
                                progress[i].setVisible(true);
                                alphabet[i].setTextFill(Color.WHITE);
                                alphabet[i].setStyle("-fx-background-color: red");
                                gamedata.addGoodGuess(guess);
                                goodguess = true;
                                discovered++;
                            }

                        }
                        if (!goodguess) {
                            gamedata.addBadGuess(guess);
                            if (hintProcessed){
                                hangmanparts[9 - (gamedata.getRemainingGuesses())].setStroke(Color.BLACK);
                                hangmanparts[9 - (gamedata.getRemainingGuesses())].setFill(Color.BLACK);
                            }
                            else if (gamedata.getRemainingGuesses()>=0) {
                                hangmanparts[9 - gamedata.getRemainingGuesses()].setStroke(Color.BLACK);
                                hangmanparts[9 - gamedata.getRemainingGuesses()].setFill(Color.BLACK);
                            }

                        }
                        success = (discovered == progress.length);
                        remains.setText(Integer.toString(gamedata.getRemainingGuesses()));
                    }
                    }

                    setGameState(GameState.INITIALIZED_MODIFIED);
                });
                if (gamedata.getRemainingGuesses() <= 0 && hintProcessed) {
                    for (int i = 0; i < hangmanparts.length;i++) {
                        hangmanparts[i].setStroke(Color.BLACK);
                        hangmanparts[i].setFill(Color.BLACK);
                    }

                }
                
                if (gamedata.getRemainingGuesses() <= 0 || success)
                    stop();



            }

            @Override
            public void stop() {
                super.stop();
                end();
            }
        };
        timer.start();
    }
    public void handleHint(){
        Random rand = new Random();
        char guess = gamedata.getRemainingLetters().get(rand.nextInt(gamedata.getRemainingLetters().size()));
            for (int i = 0; i < progress.length; i++) {
                if (gamedata.getTargetWord().charAt(i) == guess) {
                    progress[i].setVisible(true);
                    alphabet[i].setTextFill(Color.WHITE);
                    alphabet[i].setStyle("-fx-background-color: red");
                    gamedata.addGoodGuess(guess);

                    discovered++;

                }
            }

        success = (discovered == progress.length);
        gamedata.decrementRemainingGuess();
        hangmanparts[9 - (gamedata.getRemainingGuesses())].setStroke(Color.BLACK);
        hangmanparts[9 - (gamedata.getRemainingGuesses())].setFill(Color.BLACK);
        remains.setText(Integer.toString(gamedata.getRemainingGuesses()));
        Workspace gameWorkspace = (Workspace) appTemplate.getWorkspaceComponent();
        HBox alpha = (HBox) gameWorkspace.getGameTextsPane().getChildren().get(2);
        restoreAlphabet(alpha);
        gameWorkspace.getHint().setDisable(true);
        gamedata.setHintProcessed(true);
        //hintProcessed = true;


        alpha.requestFocus();

    }

    public char returnRandomChar(String word){
        Random rand = new Random();
        int n = rand.nextInt(word.length());
        return word.charAt(n);
    }


    private void restoreGUI() {
        disableGameButton();
        Workspace gameWorkspace = (Workspace) appTemplate.getWorkspaceComponent();
        gameWorkspace.reinitialize();

        HBox guessedLetters = (HBox) gameWorkspace.getGameTextsPane().getChildren().get(1);
        HBox alpha = (HBox) gameWorkspace.getGameTextsPane().getChildren().get(2);
        BorderPane figurePane = gameWorkspace.getFigurePane();
        restoreWordGraphics(guessedLetters);
        restoreAlphabet(alpha);

        alpha.requestFocus();
        HBox remainingGuessBox = gameWorkspace.getRemainingGuessBox();
        remains = new Label(Integer.toString(gamedata.getRemainingGuesses()));
        if (gamedata.isDifficult()){
            gameWorkspace.getHint().setVisible(true);
            gameWorkspace.getHint().setDisable(false);
        }
        if (gamedata.getHintProcessed()) {

            gameWorkspace.getHint().setDisable(true);
            gamedata.decrementRemainingGuess();
        }

        restoreGraphic(figurePane);
        remains = new Label(Integer.toString(gamedata.getRemainingGuesses()));
        remainingGuessBox.getChildren().addAll(new Label("Remaining Guesses: "), remains);
        success = false;
        play();
    }



    private void restoreWordGraphics(HBox guessedLetters) {
        discovered = 0;
        char[] targetword = gamedata.getTargetWord().toCharArray();
        progress = new Text[targetword.length];
        alphabet = new Button[targetword.length];
        for (int i = 0; i < progress.length; i++) {
            progress[i] = new Text(Character.toString(targetword[i]));
            alphabet[i] = new Button(Character.toString(targetword[i]));
            alphabet[i].setStyle("-fx-background-color: black");
            alphabet[i].setTextFill(Color.BLACK);
            progress[i].setVisible(gamedata.getGoodGuesses().contains(progress[i].getText().charAt(0)));
            if (progress[i].isVisible())
                discovered++;
            if (gamedata.getGoodGuesses().contains(progress[i].getText().charAt(0))) {
                alphabet[i].setTextFill(Color.WHITE);
                alphabet[i].setStyle("-fx-background-color: red");
            }
        }
        guessedLetters.getChildren().addAll(alphabet);
    }


    private void restoreGraphic(BorderPane figurePane){
        hangmanparts = new Shape[10];
        Circle head = new Circle();
        head.setCenterX(200);
        head.setCenterY(150);
        head.setRadius(50);
        hangmanparts[4] = head;
        Line line1 = new Line(50,350,150,350);
        hangmanparts[0] = line1;
        Line line2 = new Line(100,350,100,50);
        hangmanparts[1] = line2;
        Line line3 = new Line(100,50,200,50);
        hangmanparts[2] = line3;
        Line line4 = new Line(200,50,200,100);
        hangmanparts[3] = line4;
        Line line5 = new Line(200,200,200,300);
        hangmanparts[5] = line5;
        Line line6 = new Line(200,300,150,350);
        hangmanparts[6] = line6;
        Line line7 = new Line(200,300,250,350);
        hangmanparts[7] = line7;
        Line line8 = new Line(200,250,150,300);
        hangmanparts[8] = line8;
        Line line9 = new Line(200,250,250,300);
        hangmanparts[9] = line9;
        for (int i = 0; i < 10; i++) {
            hangmanparts[i].setFill(Color.TRANSPARENT);
            hangmanparts[i].setStroke(Color.TRANSPARENT);
        }

        int k = gamedata.getRemainingGuesses();
        if (k == 10) return;
        for (int i = 0; i <= (9-k);i++){
            hangmanparts[i].setStroke(Color.BLACK);
            hangmanparts[i].setFill(Color.BLACK);



        }
        figurePane.getChildren().addAll(hangmanparts);
    }

    private void restoreAlphabet(HBox alphabet){
       Button[] buttons = new Button[26];
        int count = 0;
        for (int i = 0; i < 26; i++){
            buttons[i] = (Button) alphabet.getChildren().get(i);
        }
        for (char c = 'a'; c <= 'z'; c++) {
            if (alreadyGuessed(c)){
                alphabet.getChildren().get(count).setVisible(false);
            }
            count++;
        }
    }

    private boolean alreadyGuessed(char c) {
        return gamedata.getGoodGuesses().contains(c) || gamedata.getBadGuesses().contains(c);
    }

    @Override
    public void handleNewRequest() {
        AppMessageDialogSingleton messageDialog   = AppMessageDialogSingleton.getSingleton();
        PropertyManager           propertyManager = PropertyManager.getManager();
        boolean                   makenew         = true;
        if (gamestate.equals(GameState.INITIALIZED_MODIFIED))
            try {
                makenew = promptToSave();
            } catch (IOException e) {
                messageDialog.show(propertyManager.getPropertyValue(NEW_ERROR_TITLE), propertyManager.getPropertyValue(NEW_ERROR_MESSAGE));
            }
        if (makenew) {
            appTemplate.getDataComponent().reset();                // reset the data (should be reflected in GUI)
            appTemplate.getWorkspaceComponent().reloadWorkspace(); // load data into workspace
            ensureActivatedWorkspace();                            // ensure workspace is activated
            workFile = null;                                       // new workspace has never been saved to a file
            ((Workspace) appTemplate.getWorkspaceComponent()).reinitialize();
            enableGameButton();
        }
        if (gamestate.equals(GameState.ENDED)) {
            appTemplate.getGUI().updateWorkspaceToolbar(false);
            Workspace gameWorkspace = (Workspace) appTemplate.getWorkspaceComponent();
            gameWorkspace.reinitialize();
        }

    }

    @Override
    public void handleSaveRequest() throws IOException {
        PropertyManager propertyManager = PropertyManager.getManager();
        if (workFile == null) {
            FileChooser filechooser = new FileChooser();
            Path        appDirPath  = Paths.get(propertyManager.getPropertyValue(APP_TITLE)).toAbsolutePath();
            Path        targetPath  = appDirPath.resolve(APP_WORKDIR_PATH.getParameter());
            filechooser.setInitialDirectory(targetPath.toFile());
            filechooser.setTitle(propertyManager.getPropertyValue(SAVE_WORK_TITLE));
            String description = propertyManager.getPropertyValue(WORK_FILE_EXT_DESC);
            String extension   = propertyManager.getPropertyValue(WORK_FILE_EXT);
            ExtensionFilter extFilter = new ExtensionFilter(String.format("%s (*.%s)", description, extension),
                    String.format("*.%s", extension));
            filechooser.getExtensionFilters().add(extFilter);
            File selectedFile = filechooser.showSaveDialog(appTemplate.getGUI().getWindow());
            if (selectedFile != null)
                save(selectedFile.toPath());
        } else
            save(workFile);
    }

    @Override
    public void handleLoadRequest() throws IOException {
        boolean load = true;
        if (gamestate.equals(GameState.INITIALIZED_MODIFIED))
            load = promptToSave();
        if (load) {
            PropertyManager propertyManager = PropertyManager.getManager();
            FileChooser     filechooser     = new FileChooser();
            Path            appDirPath      = Paths.get(propertyManager.getPropertyValue(APP_TITLE)).toAbsolutePath();
            Path            targetPath      = appDirPath.resolve(APP_WORKDIR_PATH.getParameter());
            filechooser.setInitialDirectory(targetPath.toFile());
            filechooser.setTitle(propertyManager.getPropertyValue(LOAD_WORK_TITLE));
            String description = propertyManager.getPropertyValue(WORK_FILE_EXT_DESC);
            String extension   = propertyManager.getPropertyValue(WORK_FILE_EXT);
            ExtensionFilter extFilter = new ExtensionFilter(String.format("%s (*.%s)", description, extension),
                    String.format("*.%s", extension));
            filechooser.getExtensionFilters().add(extFilter);
            File selectedFile = filechooser.showOpenDialog(appTemplate.getGUI().getWindow());
            if (selectedFile != null && selectedFile.exists())
                load(selectedFile.toPath());
            restoreGUI(); // restores the GUI to reflect the state in which the loaded game was last saved
        }
    }

    @Override
    public void handleExitRequest() {
        try {
            boolean exit = true;
            if (gamestate.equals(GameState.INITIALIZED_MODIFIED))
                exit = promptToSave();
            if (exit)
                System.exit(0);
        } catch (IOException ioe) {
            AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();
            PropertyManager           props  = PropertyManager.getManager();
            dialog.show(props.getPropertyValue(SAVE_ERROR_TITLE), props.getPropertyValue(SAVE_ERROR_MESSAGE));
        }
    }

    private void ensureActivatedWorkspace() {
        appTemplate.getWorkspaceComponent().activateWorkspace(appTemplate.getGUI().getAppPane());
    }

    private boolean promptToSave() throws IOException {
        PropertyManager            propertyManager   = PropertyManager.getManager();
        YesNoCancelDialogSingleton yesNoCancelDialog = YesNoCancelDialogSingleton.getSingleton();

        yesNoCancelDialog.show(propertyManager.getPropertyValue(SAVE_UNSAVED_WORK_TITLE),
                propertyManager.getPropertyValue(SAVE_UNSAVED_WORK_MESSAGE));

        if (yesNoCancelDialog.getSelection().equals(YesNoCancelDialogSingleton.YES))
            handleSaveRequest();

        return !yesNoCancelDialog.getSelection().equals(YesNoCancelDialogSingleton.CANCEL);
    }

    /**
     * A helper method to save work. It saves the work, marks the current work file as saved, notifies the user, and
     * updates the appropriate controls in the user interface
     *
     * @param target The file to which the work will be saved.
     * @throws IOException
     */
    private void save(Path target) throws IOException {
        appTemplate.getFileComponent().saveData(appTemplate.getDataComponent(), target);
        workFile = target;
        setGameState(GameState.INITIALIZED_UNMODIFIED);
        AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();
        PropertyManager           props  = PropertyManager.getManager();
        dialog.show(props.getPropertyValue(SAVE_COMPLETED_TITLE), props.getPropertyValue(SAVE_COMPLETED_MESSAGE));
    }

    /**
     * A helper method to load saved game data. It loads the game data, notified the user, and then updates the GUI to
     * reflect the correct state of the game.
     *
     * @param source The source data file from which the game is loaded.
     * @throws IOException
     */
    private void load(Path source) throws IOException {
        // load game data
        appTemplate.getFileComponent().loadData(appTemplate.getDataComponent(), source);
        // set the work file as the file from which the game was loaded
        workFile = source;
        // notify the user that load was successful
        AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();
        PropertyManager           props  = PropertyManager.getManager();
        dialog.show(props.getPropertyValue(LOAD_COMPLETED_TITLE), props.getPropertyValue(LOAD_COMPLETED_MESSAGE));
        setGameState(GameState.INITIALIZED_UNMODIFIED);
        Workspace gameworkspace = (Workspace) appTemplate.getWorkspaceComponent();
        ensureActivatedWorkspace();
        gameworkspace.reinitialize();
        gamedata = (GameData) appTemplate.getDataComponent();
    }
}