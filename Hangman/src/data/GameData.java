package data;

import apptemplate.AppTemplate;
import components.AppDataComponent;
import controller.GameError;

import java.io.IOException;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Random;
import java.util.Set;
import java.util.stream.Stream;

/**
 * @author Ritwik Banerjee
 * @author omar amin
 */
public class GameData implements AppDataComponent {

    public static final  int TOTAL_NUMBER_OF_GUESSES_ALLOWED = 10;
    private static final int TOTAL_NUMBER_OF_STORED_WORDS    = 330622;

    private String         targetWord;
    private Set<Character> goodGuesses;
    private Set<Character> badGuesses;
    private int            remainingGuesses;
    public  AppTemplate    appTemplate;
    public Boolean HintProcessed = false;

    public GameData(AppTemplate appTemplate) {
        this(appTemplate, false);
    }

    public GameData(AppTemplate appTemplate, boolean initiateGame) {
        if (initiateGame) {
            this.appTemplate = appTemplate;
            this.targetWord = setTargetWord();
            this.goodGuesses = new HashSet<>();
            this.badGuesses = new HashSet<>();
            this.remainingGuesses = TOTAL_NUMBER_OF_GUESSES_ALLOWED;
            this.HintProcessed = false;
        } else {
            this.appTemplate = appTemplate;
        }
    }
    public Boolean isDifficult(){
        String word = targetWord;
        ArrayList<Character> newList = new ArrayList();
        for (int i = 0; i < word.length(); i++){
            if (!newList.contains(word.charAt(i)))
                newList.add(word.charAt(i));


        }
        return (newList.size() > 7);


    }

    public Boolean getHintProcessed() {
        return HintProcessed;
    }

    public void setHintProcessed(Boolean hintProcessed) {
        HintProcessed = hintProcessed;
    }

    public void init() {
        this.targetWord = setTargetWord();
        this.goodGuesses = new HashSet<>();
        this.badGuesses = new HashSet<>();
        this.remainingGuesses = TOTAL_NUMBER_OF_GUESSES_ALLOWED;
    }

    @Override
    public void reset() {
        this.targetWord = null;
        this.goodGuesses = new HashSet<>();
        this.badGuesses = new HashSet<>();
        this.remainingGuesses = TOTAL_NUMBER_OF_GUESSES_ALLOWED;
        appTemplate.getWorkspaceComponent().reloadWorkspace();
    }

    public String getTargetWord() {
        return targetWord;
    }

    private String setTargetWord() {
        URL wordsResource = getClass().getClassLoader().getResource("words/words.txt");
        assert wordsResource != null;

        int toSkip = new Random().nextInt(TOTAL_NUMBER_OF_STORED_WORDS);
        try (Stream<String> lines = Files.lines(Paths.get(wordsResource.toURI()))) {
            String word = lines.skip(toSkip).findFirst().get();
            Boolean legalword = true;
            while (!isAlpha(word))
                word = lines.skip(toSkip).findFirst().get();
            return word;
        } catch (IOException | URISyntaxException e) {
            e.printStackTrace();
            System.exit(1);
        }

        throw new GameError("Unable to load initial target word.");
    }

    public GameData setTargetWord(String targetWord) {
        this.targetWord = targetWord;
        return this;
    }
    public boolean isAlpha(String name) {
        return name.matches("[a-zA-Z]+");
    }

    public Set<Character> getGoodGuesses() {
        return goodGuesses;
    }

    public ArrayList<Character> getRemainingLetters(){
        ArrayList<Character> remainingLetters = new ArrayList<Character>();

        for (int i = 0; i < targetWord.length(); i++){
            if (!goodGuesses.contains(targetWord.charAt(i)))
                remainingLetters.add(targetWord.charAt(i));

        }
        return remainingLetters;
    }

    public GameData setGoodGuesses(Set<Character> goodGuesses) {
        this.goodGuesses = goodGuesses;
        return this;
    }

    public Set<Character> getBadGuesses() {
        return badGuesses;
    }

    public GameData setBadGuesses(Set<Character> badGuesses) {
        this.badGuesses = badGuesses;
        return this;
    }

    public int getRemainingGuesses() {
        return remainingGuesses;
    }

    public void addGoodGuess(char c) {
        goodGuesses.add(c);
    }

    public void decrementRemainingGuess(){
        remainingGuesses--;
    }

    public void addBadGuess(char c) {
        if (!badGuesses.contains(c)) {
            badGuesses.add(c);
            remainingGuesses--;
        }
    }


}
